# Copyright 2018 Dark Kirb
# Distributed under the terms of the MIT License

EAPI=5

DESCRIPTION="Tools for extracting 3DS ROM files"
HOMEPAGE="https://github.com/profi200/Project_CTR/tree/master/ctrtool"
SRC_URI="https://github.com/profi200/Project_CTR/archive/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE=""

S=${WORKDIR}/Project_CTR-${PV}/ctrtool

src_configure() {
	true
}
src_install() {
	mkdir -pv ${D}/usr/bin
	cp ctrtool ${D}/usr/bin/
}
