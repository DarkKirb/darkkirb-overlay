# Copyright 2018 Dark Kirb
# Distributed under the terms of the MIT License

EAPI=5
EGIT_REPO_URI="https://github.com/profi200/Project_CTR.git"
inherit git-r3

DESCRIPTION="Tools for creating 3DS ROM files"
HOMEPAGE="https://github.com/profi200/Project_CTR/tree/master/makerom"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE=""

S=${WORKDIR}/${PN}-${PV}/makerom

src_configure() {
	true
}
src_install() {
	mkdir -pv ${D}/usr/bin
	cp makerom ${D}/usr/bin/
}
